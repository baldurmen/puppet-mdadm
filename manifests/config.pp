class mdadm::config () {

  file {
    '/usr/local/sbin/luks':
      ensure  => present,
      content => "${::mdadm::command}",
      owner   => 'root',
      group   => 'root',
      mode    => '0700';
  }

  if $array != 'undef' {
    file {
      '/etc/mdadm/mdadm.conf':
        content => epp('mdadm/mdadm.conf.epp', {
          'array' => $::mdadm::array,
        }),
        owner   => 'root',
        group   => 'root',
        mode    => '0644';
    }
  }

  package {
    'mdadm':
      ensure => present;
  }
}
